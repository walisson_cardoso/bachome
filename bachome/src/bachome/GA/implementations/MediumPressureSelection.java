
package bachome.GA.implementations;

import java.util.Arrays;

import bachome.GA.GAUse;
import bachome.GA.Individual;
import bachome.GA.Population;
import bachome.GA.interfaces.SurvivorSelection;

public class MediumPressureSelection implements SurvivorSelection{
    
    
    @Override
    public Population doSelection(Population pop, Population nextPop){
        
        int nGenes = pop.getNGenes();
        
        int nInds1    = pop.getNIndividuals();
        int nInds2    = nextPop.getNIndividuals();
        double fits[] = new double[nInds1+nInds2];
        
        int totalPop = nInds1+nInds2;
        double minFitness = -1;
        int index;
        
        if(totalPop > GAUse.getMaxPopSize()){
        
            for(int i = 0; i < nInds1; i++)
                fits[i] = pop.getIndividual(i).getFitness();

            for(int i = 0; i < nInds2; i++)
                fits[i+nInds1] = nextPop.getIndividual(i).getFitness();
        
            Arrays.sort(fits);
            index = totalPop-GAUse.getMaxPopSize();
            minFitness = fits[index];
        }
        
        int size   = 0;
        for(int i = 0; i < nInds1; i++){
            int time   = pop.getIndividual(i).getLifeTime();
            double fit = pop.getIndividual(i).getFitness();
            if(time <= GAUse.getMaxTimetoLive() && fit >= minFitness)
                size++;
        }
        
        for(int i = 0; i < nInds2; i++){
            int time   = nextPop.getIndividual(i).getLifeTime();
            double fit = nextPop.getIndividual(i).getFitness();
            if(time <= GAUse.getMaxTimetoLive() && fit >= minFitness)
                size++;
        }
        
        Population survivors = new Population(size, nGenes);
        if(size == 0) //Population died
            return survivors;
        index = 0;
        
        for(int i = 0; i < nInds1; i++){
            int time   = pop.getIndividual(i).getLifeTime();
            double fit = pop.getIndividual(i).getFitness();
            if(time <= GAUse.getMaxTimetoLive() && fit >= minFitness){
                Individual ind = new Individual();
                ind.copy(pop.getIndividual(i));
                survivors.setIndividual(ind, index);
                index++;
            }
        }
        
        for(int i = 0; i < nInds2; i++){
            int time   = nextPop.getIndividual(i).getLifeTime();
            double fit = nextPop.getIndividual(i).getFitness();
            if(time <= GAUse.getMaxTimetoLive() && fit >= minFitness){
                Individual ind = new Individual();
                ind.copy(nextPop.getIndividual(i));
                survivors.setIndividual(ind, index);
                index++;
            }
        }
        /*
        int nInds1    = pop.getNIndividuals();
        int nInds2    = nextPop.getNIndividuals();
        
        int size   = 0;
        for(int i = 0; i < nInds1; i++){
            int time   = pop.getIndividual(i).getLifeTime();
            double fit = pop.getIndividual(i).getFitness();
            if(time <= GAUse.getMaxTimetoLive())
                size++;
        }
        
        for(int i = 0; i < nInds2; i++){
            int time   = nextPop.getIndividual(i).getLifeTime();
            double fit = nextPop.getIndividual(i).getFitness();
            if(time <= GAUse.getMaxTimetoLive())
                size++;
        }
        
        Population survivors = new Population(size, 5);
        if(size == 0) //Population died
            return survivors;
        int index = 0;
        
        for(int i = 0; i < nInds1; i++){
            int time   = pop.getIndividual(i).getLifeTime();
            double fit = pop.getIndividual(i).getFitness();
            if(time <= GAUse.getMaxTimetoLive()){
                Individual ind = new Individual();
                ind.copy(pop.getIndividual(i));
                survivors.setIndividual(ind, index);
                index++;
            }
        }
        
        for(int i = 0; i < nInds2; i++){
            int time   = nextPop.getIndividual(i).getLifeTime();
            double fit = nextPop.getIndividual(i).getFitness();
            if(time <= GAUse.getMaxTimetoLive()){
                Individual ind = new Individual();
                ind.copy(nextPop.getIndividual(i));
                survivors.setIndividual(ind, index);
                index++;
            }
        }*/
        
        return survivors;
    }
}
