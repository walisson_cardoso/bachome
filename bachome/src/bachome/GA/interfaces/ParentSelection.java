
package bachome.GA.interfaces;

import bachome.GA.Individual;
import bachome.GA.Population;


public interface ParentSelection {
    public Individual select(Population pop);
}
