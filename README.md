# Bachome (Bacteria's Home)

O software Bachome expressa alguns dos conceitos chave da seleção natural em ambiente gráﬁco. O software simula o desenvolvimento de uma população de micro-organismos cujo comportamento é descrito por um algoritmo genético, uma classe de algoritmos evolucionários.

## Objetivos deste software

* Utilizar um algoritmo genético para evoluir uma população de “bactérias”. Bactérias são a metáfora utilizada como representação da população apresentada ao usuário. A apresentação da população será atualizada a cada iteração do AG. 
* Fornecer ambientação gráﬁca para imersão do usuário. O ambiente gráﬁco é colorido, e visualmente interessante,fornecendo fácil diferenciação entre as classes de Bactérias-indivíduo utilizadas e a conﬁguração populacional.
* Permitir ao usuário interagir com diversos aspectos da aplicação por meio da aplicação gráﬁca. Inclui-se como objetivo permitir ao usuário escolher um cenário para a população e os fatores de pressão do ambiente. Os usuários mais avançados se beneﬁciarão podendo escolher também aspectos relacionados ao algoritmo genético como tipo de cruzamento, mutação e seleções de sobrevivência e cruzamento.


## Detalhes da simulação

Exitem três tipos de formatos que um indivíduo pode assumir: formato alongado com presença de ﬂagelo, formato esférico e formato triangular. Apesar de os formatos serem baseados em classes de bactérias reais, são tratados aqui de forma meramente ilustrativa e nenhum estudo é realizado ou diferenciação é feita em relação a este tema. Da mesma forma, existem três cores que os indivíduos podem assumir: cinza, verde e vermelho.

De acordo com o tipo de fator de seleção presente, uma ou mais classes de indivíduos terão mais resistência. De forma geral as características podem ser competitivas em relação ao:

* Formato:

~~~
Alongado: favorecido quando há seleção por predadores
Triangular: favorecido quando há seleção por recursos
Circular: não representa quaisquer vantagens
~~~

* Cor:

~~~
Vermelho: resistente a variação de temperatura
Verde: resistente a aplicação de antibiótico
Cinza: resistente a variação de PH
~~~

Os referidos fatores de seleção podem ser selecionados em tempo real pelos usuários da aplicação. O algoritmo genético deve colher estas informações para aplicar na sua função de avaliação.


## Detalhes da implementação

O diagrama de casos de uso ilustrado na figura abaixo foi desenvolvido para listar as funcionalidades principais que o sistema deveria conter. O usuário representado na ﬁgura pode ser um aluno do ensino básico que executa a aplicação por vontade própria e estaria interessado em testar a aplicação dos diferentes operadores de seleção e mutação da simulação, pode ser um tutor mostrando a simulação para um grupo de alunos e usar as operações disponíveis para auxiliar em sua explanação à respeito da seleção natural, ou ainda um estudante do nível superior, interessado em investigar o funcionamento do AG e, portanto, operar nas opções disponíveis para alteração de módulos e funções de avaliação.

![picture](imagens_readme/UseCaseDiagram.png)

Com base na série de itens listados como requisitos, foi desenvolvida uma estrutura que pode ser segmentada em três partes: algoritmo genético, interface gráﬁca e classe de controle, como ilustrado pela figura abaixo.

![picture](imagens_readme/packages.png)

Em 1960 algoritmos genéticos foram inventados por John Holland, e posteriormente desenvolvidos por ele e seus estudantes. Ao contrário de estratégias e programação evolutivas, o objetivo inicial de Holland não era criar um algoritmo para resolver problemas especíﬁcos, mas estudar o fenômeno da adaptação, como ele acontece na natureza e desenvolver formas pelas quais adaptação natural poderia ser importada para dentro de computadores. Os algoritmos genéticos utilizam um workﬂow básico para o seu funcionamento representado no digrama de atividades abaixo.

![picture](imagens_readme/ag_diagram.png)

Esta implementação não utiliza por padrão o algoritmo genético canônico, mas representação real e seleção de pais por torneio. A codiﬁcação real foi escolhida pois cada indivíduo da aplicação possui algumas características, chamadas níveis de resistência, no domínio dos reais. O mapeamento entre uma solução e o problema torna-se direto. No entando, vale ressaltar que cada operador do AG é representado por uma interface seguindo o padrão de projetos strategy. O objetivo é tornar possível alternar entre os diferentes tipos de operadores em tempo de execução e facilitar a implementação de novos tipos sob a interface. O usuário pode também adicionar novas operações ao software se assim o desejar.

A baixo segue o diagrama de classes geral da aplicação.

![picture](imagens_readme/class_diagram.png)


## Executando o código

A pasta bachome neste diretório é um projeto Netbeans na Linguagem Java. Recomendamos a utilização da IDE Netbeans 7.1.2 ou superior.

Para executar, basta importar o projeto e executá-lo normalmente. é necessário apenas observar a hierarquia estabelecida no projeto.