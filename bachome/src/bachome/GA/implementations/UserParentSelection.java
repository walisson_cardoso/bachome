
package bachome.GA.implementations;

import bachome.GA.Individual;
import bachome.GA.Population;
import bachome.GA.interfaces.ParentSelection;
import bachome.RandomPool;

public class UserParentSelection implements ParentSelection{
    
    @Override
    public Individual select(Population pop){
        Individual selected = new Individual();
        selected.copy(pop.getIndividual(0));
        
        //You can implement this class
        
        return selected;
    }
}
