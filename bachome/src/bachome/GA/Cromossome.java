
package bachome.GA;

import bachome.RandomPool;

public class Cromossome {
    private int nGenes;
    private Gene genes[];
    
    public Cromossome(){
        
    }
    
    public Cromossome(int nGenes) {
        if(nGenes > 0){
            this.nGenes = nGenes;
            this.genes = new Gene[nGenes];
            for(int i = 0; i < nGenes; i++)
                this.genes[i] = new Gene(0);
        }
    }
    
    public void initGenes(){
        if(genes.length > 0){
            for(int i = 0; i < genes.length; i++){
                double value = RandomPool.r().nextDouble(true, true);
                genes[i].setValue(value);
            }
        }
    }
    
    public void initGenes(String shape){
        if(genes.length > 0 && shape.length() == genes.length){
            for(int i = 0; i < genes.length; i++){
                if(shape.charAt(i) == 'r'){
                    //Random value for position
                    double value = RandomPool.r().nextDouble(true, true);
                    genes[i].setValue(value);
                }else if(shape.charAt(i) == '0'){
                    //Set position as zero
                    double value = 0;
                    genes[i].setValue(value);
                }else if(shape.charAt(i) == '1'){
                    //Set position as one
                    double value = 1;
                    genes[i].setValue(value);
                }
            }
        }
    }
    
    public void setGene(Gene g, int index){
        if(index >= 0 && index < genes.length)
            genes[index] = g;
        else
            System.out.println("Gene index does not exist on setGene");
    }
    
    public Gene getGene(int index){
        if(index >= 0 && index < genes.length)
            return genes[index];
        else
            System.out.println("Gene index does not exist on getGene");
        return null;
    }
    
    public int getNGenes(){
        return nGenes;
    }
}
