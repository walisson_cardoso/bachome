
package bachome.GA.interfaces;

import bachome.GA.Individual;

public interface Crossover {
    public Individual doCrossover(Individual parent1, Individual parent2);
    public void       setProbability(double probability);
    public double     getProbability();
}
