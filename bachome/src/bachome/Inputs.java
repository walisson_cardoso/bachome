
package bachome;

import java.util.ArrayList;
import java.util.List;

public class Inputs {
    
    private boolean changed = false;
    
    private int speed = 1;
    //General configuration
    private boolean mut_color       = false;
    private boolean mut_shape       = false;
    private int  mut_color_press = 1;
    private int  mut_shape_press = 1;
    
    private boolean selec_food       = false;
    private boolean selec_pred       = false;
    private int  selec_food_press = 1;
    private int  selec_pred_press = 1;
    
    private boolean apply_antibio = false;
    private boolean apply_temper  = false;
    private boolean apply_ph      = false;
    private int antibio_press = 1;
    private int temper_press  = 5;
    private int ph_press      = 5;

    //AG configuration
    private double crossProb = 1.0;
    private double mutProb   = 0.3;
    private String parentSelection = "Torneio";
    private String representation = "Binário";
    private String fitness = "Natural"; //
    private String Mutation = "Inversão";
    private String Crossover = "Um Ponto";
    private String SurvivorSelection = "Natural";
    private int sizePop = 20;
    private int rigSize = 3;
    private int maxGen = 200;
    
    private List<VisualElement> elements = new ArrayList<>();
    
    
    public Inputs() {
        
    }

    //Bacs grandes comem melhor
    //Bacs com fragelo fogem de predadores.
    //Bacs verdes são melhores com antibio
    //Bacs vemelhas são melhores com temperatura
    //Bacs brancas são melhores com PH

    public void setSpeed(int speed) {
        this.speed = speed;
        changed = true;
    }
    
    public void setMut_color(boolean mut_color) {
        this.mut_color = mut_color;
        changed = true;
    }

    public void setMut_shape(boolean mut_shape) {
        this.mut_shape = mut_shape;
        changed = true;
    }

    public void setMut_color_press(int mut_color_press) {
        this.mut_color_press = mut_color_press;
        changed = true;
    }

    public void setMut_shape_press(int mut_shape_press) {
        this.mut_shape_press = mut_shape_press;
        changed = true;
    }

    public void setSelec_food(boolean selec_food) {
        this.selec_food = selec_food;
        changed = true;
    }

    public void setSelec_pred(boolean selec_pred) {
        this.selec_pred = selec_pred;
        changed = true;
    }

    public void setSelec_food_press(int selec_food_press) {
        this.selec_food_press = selec_food_press;
        changed = true;
    }

    public void setSelec_pred_press(int selec_pred_press) {
        this.selec_pred_press = selec_pred_press;
        changed = true;
    }

    public void setApply_antibio(boolean apply_antibio) {
        this.apply_antibio = apply_antibio;
        changed = true;
    }

    public void setApply_temper(boolean apply_temper) {
        this.apply_temper = apply_temper;
        changed = true;
    }

    public void setApply_ph(boolean apply_ph) {
        this.apply_ph = apply_ph;
        changed = true;
    }

    public void setAntibio_press(int antibio_press) {
        this.antibio_press = antibio_press;
        changed = true;
    }

    public void setTemper_press(int temper_press) {
        this.temper_press = temper_press;
        changed = true;
    }

    public void setPh_press(int ph_press) {
        this.ph_press = ph_press;
        changed = true;
    }

    public void setCrossProb(double crossProb) {
        this.crossProb = crossProb;
        changed = true;
    }

    public void setMutProb(double mutProb) {
        this.mutProb = mutProb;
        changed = true;
    }

    public void setParentSelection(String parentSelection) {
        this.parentSelection = parentSelection;
        changed = true;
    }

    public void setRepresentation(String representation) {
        this.representation = representation;
        changed = true;
    }

    public void setFitness(String fitness) {
        this.fitness = fitness;
        changed = true;
    }

    public void setMutation(String Mutation) {
        this.Mutation = Mutation;
        changed = true;
    }

    public void setCrossover(String Crossover) {
        this.Crossover = Crossover;
        changed = true;
    }

    public void setSurvivorSelection(String SurvivorSelection) {
        this.SurvivorSelection = SurvivorSelection;
        changed = true;
    }

    public void setSizePop(int sizePop) {
        this.sizePop = sizePop;
        changed = true;
    }

    public void setRigSize(int rigSize) {
        this.rigSize = rigSize;
        changed = true;
    }
    
    public void setMaxGen(int maxGen) {
        this.maxGen = maxGen;
        changed = true;
    }

    public void setElements(List<VisualElement> elements) {
        this.elements = elements;
    }
    
    
    
    
    ///////////////////////////////////////////
    // Getters

    public boolean changeFlag() {
        if(changed){
            changed = false;
            return true;
        }
        return false;
    }
    
    public int getSpeed() {
        return speed;
    }

    public boolean doMut_color() {
        return mut_color;
    }

    public boolean doMut_shape() {
        return mut_shape;
    }

    public int getMut_color_press() {
        return mut_color_press;
    }

    public int getMut_shape_press() {
        return mut_shape_press;
    }

    public boolean isSelec_food() {
        return selec_food;
    }

    public int getSelec_food_press() {
        return selec_food_press;
    }

    public int getSelec_pred_press() {
        return selec_pred_press;
    }

    public boolean isApply_antibio() {
        return apply_antibio;
    }

    public boolean isApply_temper() {
        return apply_temper;
    }

    public boolean isApply_ph() {
        return apply_ph;
    }

    public int getAntibio_press() {
        return antibio_press;
    }

    public int getTemper_press() {
        return temper_press;
    }

    public int getPh_press() {
        return ph_press;
    }

    public boolean isSelec_pred() {
        return selec_pred;
    }
    
    public boolean isMut_color() {
        return mut_color;
    }

    public boolean isMut_shape() {
        return mut_shape;
    }

    public double getCrossProb() {
        return crossProb;
    }

    public double getMutProb() {
        return mutProb;
    }

    public String getParentSelection() {
        return parentSelection;
    }

    public String getRepresentation() {
        return representation;
    }

    public String getFitness() {
        return fitness;
    }

    public String getMutation() {
        return Mutation;
    }

    public String getCrossover() {
        return Crossover;
    }

    public String getSurvivorSelection() {
        return SurvivorSelection;
    }

    public int getSizePop() {
        return sizePop;
    }

    public int getRigSize() {
        return rigSize;
    }
    
    public int getMaxGen() {
        return maxGen;
    }

    public List<VisualElement> getElements() {
        return elements;
    }
}
