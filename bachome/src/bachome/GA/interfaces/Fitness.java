
package bachome.GA.interfaces;

import bachome.GA.Individual;
import bachome.Inputs;

public interface Fitness {
    public double     evaluate(Individual ind, Inputs inputs);
}
