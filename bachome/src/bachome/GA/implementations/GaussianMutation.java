
package bachome.GA.implementations;

import bachome.GA.Cromossome;
import bachome.GA.Gene;
import bachome.GA.Individual;
import bachome.GA.interfaces.Mutation;
import bachome.RandomPool;


public class GaussianMutation implements Mutation{
    
    private double probability;
    
    @Override
    public Individual doMutation(Individual ind, String shape){
        
        Cromossome crm = ind.getCromossome();
        int position   = RandomPool.r().nextInt(crm.getNGenes());
        
        Gene gene      = crm.getGene(position);
        
        if(RandomPool.r().nextDouble() < probability){
            
            double value = 0;
            
            if(shape.charAt(position) == 'r'){
                value = gene.getValue() + RandomPool.r().nextGaussian() * 0.5;
                while(value < 0 || value > 1){
                    if(value > 1)
                        value = 2 - value;
                    if(value < 0)
                        value = -value;
                }
            }
            
            gene.setValue(value);
            crm.setGene(gene, position);
            ind.setCromossome(crm);
        }
        
        return ind;
    }
    
    @Override
    public void setProbability(double probability){
        
        if(probability >= 0 && probability <= 1)
            this.probability = probability;
        else
            System.out.println("setProbability: invalid probability");
    }
    
    @Override
    public double getProbability(){
        
        return this.probability;
    }
}
