
package bachome.GA;

public class GAUse {
    private static boolean useElitism = false;
    private static long    lastID = 0;
    private static int     timetoLive = 5;
    private static double  mediumPressure = 0.5;
    private static int     currentPopSize = 0;
    private static int     maxPopSize = 20;
    
    public static void setElitism(boolean enable){
        useElitism = enable;
    }
    
    public static boolean elitism(){
        return useElitism;
    }
    
    public static void maxTimetoLive(int time){
        if(time > 0)
            timetoLive = time;
        else
            System.out.println("GlobalUse: invalide time to live");
    }
    
    public static int getMaxTimetoLive(){
        return timetoLive;
    }
    
    public static long assignID(){
        lastID = lastID % 1000000000 + 1; //Avoid an improbable overflow
        return lastID;
    }
    
    public static void setMediumPressure(double press){
        if(press > 0 && press < 1)
            mediumPressure = press;
        else
            System.out.println("setMediumPressure: invalid parameter");
    }
    
    public static double getMediumPressure(){
        return mediumPressure;
    }
    
    public static void setCurrentPopSize(int popSize){
        currentPopSize = popSize;
    }
    
    public static int getCurrentPopSize(){
        return currentPopSize;
    }
    
    public static void setMaxPopSize(int max){
        if(max > 0)
            maxPopSize = max;
        else
            System.out.println("setMaxPopSize: invalid parameter");
    }
    
    public static int getMaxPopSize(){
        return maxPopSize;
    }
}
