
package bachome.GA;

public class Gene {
    private double value;
    
    public Gene(){
        
    }
    
    public Gene(double val){
        setValue(val);
    }
    
    public void setValue(double value){
        if(value >= 0 && value <= 1)
            this.value = value;
        else{
            System.out.println("Setting a out of range value for a real gene. Adjusting to zero");
            this.value = 0;
        }
    }
    
    public double getValue(){
        return value;
    }
}
