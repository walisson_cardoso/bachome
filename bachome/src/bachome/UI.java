
package bachome;

import bachome.GA.GAUse;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.plaf.basic.BasicArrowButton;


public class UI extends JPanel {

    //simulation related parameters
    Inputs inputs = new Inputs();
    
    
    //General variables
    private int sizeBar = 60;
    private int width = 1280;
    private int height = 720 + sizeBar;
    private int bacImageWidth = 100+ sizeBar;
    private int bacImageHeight = 100+ sizeBar;
    
    private String[] bacNames = {"CircGray", "CircGreen", "CircRed",
        "CoryGray", "CoryGreen", "CoryRed", "OvalGray",
        "OvalGreen", "OvalRed"};
    
    private boolean dirty = true;
    private boolean drawPop = false;
    private Sprite background = new Sprite(0, 0, "images/backGround.png");
    private Sprite t = new Sprite(400, 400, "images/bacCoryGray.png");
    private JFrame frame = new JFrame("Bachome");
    private List<VisualElement> elemList = new ArrayList<>();
    private int maxOneType = GAUse.getMaxPopSize() + 5;
    private int nTypes = bacNames.length;
    private Sprite bacterias[] =  new Sprite[nTypes*maxOneType];
    
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    public UI() {
        
        JMenuBar JMB = new JMenuBar();
        frame.add(new JMenuBar());
	frame.setJMenuBar(JMB);
	JMenu edit = new JMenu("Editar");
	JMenu sobre = new JMenu("Sobre");
	JMB.add(edit); JMB.add(sobre);
        
	frame.add(this);
	frame.setSize(width, height);
	frame.setVisible(true);
        frame.setLocationRelativeTo(null);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        System.out.println("Loading Sprites...");
        
        for(int i = 0; i < nTypes*maxOneType; i++){
            String location = "images/bac" + bacNames[i/maxOneType] + ".png";
            bacterias[i] = new Sprite(-100, -100, location);
        }
        
        System.out.println("Sprites loaded!");
        
        drawPop = true;
        
        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent me) {
                //mouse = me.getPoint();
            }
        });
        
                
        frame.addComponentListener(new ComponentListener() {

            @Override
            public void componentResized(ComponentEvent ce) {
                if(width != frame.getWidth() || height != frame.getHeight()){
                    width = frame.getWidth();
                    height = frame.getHeight();
                    //frame.setBounds(frame.getX(), frame.getY(), width, height);
                    background.resize(width, height - sizeBar);
                }
            }

            @Override
            public void componentMoved(ComponentEvent ce) {
                
            }

            @Override
            public void componentShown(ComponentEvent ce) {
                
            }

            @Override
            public void componentHidden(ComponentEvent ce) {
                
            }
        });
        
        edit.add(Configurar);
        edit.add(Botao);
        
        sobre.add(ajuda);
        sobre.add(info);
        
        //Sound.BACK.loop();
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setClip(0,0,frame.getWidth(), frame.getHeight());
    	Graphics2D g2d = (Graphics2D) g;
        
        background.paint(g2d);
        
        if(!drawPop) return;
        
        List<VisualElement> newVelList = inputs.getElements();
        
        for(int i = 0; i < newVelList.size(); i++){
            int index = -1;
            for(int j = 0; j < elemList.size(); j++){
                if(newVelList.get(i).getId() == elemList.get(j).getId()){
                    index = j;
                }
            }
            if(index == -1){
                int x           = RandomPool.r().nextInt(width - bacImageWidth);
                int y           = RandomPool.r().nextInt(height - bacImageHeight);
                int orientation = RandomPool.r().nextInt(4);
                double angle    = RandomPool.r().nextDouble();
                
                newVelList.get(i).setX(x);
                newVelList.get(i).setY(y);
                newVelList.get(i).setOrientation(orientation);
                newVelList.get(i).setAngle(angle);
            }else{
                int x           = elemList.get(index).getX();
                int y           = elemList.get(index).getY();
                int orientation = elemList.get(index).getOrientation();
                double angle    = elemList.get(index).getAngle();
                
                newVelList.get(i).setX(x);
                newVelList.get(i).setY(y);
                newVelList.get(i).setOrientation(orientation);
                newVelList.get(i).setAngle(angle);
            }
        }
        
        elemList = new ArrayList<VisualElement>(newVelList);

        int lastOnes[] = new int[nTypes];
        
        for(int i = 0; i < elemList.size(); i++){
            int x = elemList.get(i).getX();
            int y = elemList.get(i).getY();
            
            for(int j = 0; j < nTypes; j++){
                if(elemList.get(i).getType().equals(bacNames[j])){
                    int index = j*maxOneType+lastOnes[j];
                    bacterias[index].setPos(x, y);
                    bacterias[index].paint(g2d);
                    lastOnes[j]++;
                    break;
                }
            }
        }
    }
    
    
    //Botões de Ação
    
    Action Configurar = new AbstractAction("Configurar") {
	public void actionPerformed(ActionEvent e) {
            configurar();
	}
    };
    
    Action Botao = new AbstractAction("Algoritmo Genético") {
	public void actionPerformed(ActionEvent e) {
              configurarAG();  
	}
    };
    
    Action ajuda = new AbstractAction("Ajuda"){
        public void actionPerformed(ActionEvent e) {
            String ajuda = "Texto de ajuda";
            
            JOptionPane.showMessageDialog(null, ajuda);
        }
    };
    
    Action info = new AbstractAction("Info"){
        public void actionPerformed(ActionEvent e) {
            String info = "Sobre o projeto";
            
            JOptionPane.showMessageDialog(null, info);
        }
    };
    
    //Funções Auxiliares
    
    private boolean isDirty(){
        return dirty;
    }
    
    private void configurar() {
        
        
        JPanel panel = new JPanel(new GridLayout(0, 1, 10, 10));
        
        ///////////////////
        //File Management
        JPanel filePanel = new JPanel(new GridLayout(4, 1, 4, 6));
        JButton saveBut   = new JButton("Salvar");
        JButton loadBut = new JButton("Carregar");
        JTextField fileName = new JTextField("configuracao.mcvl");
        
        JPanel inFilePanel = new JPanel(new GridLayout(1, 2, 3, 3));
        inFilePanel.add(saveBut);
        inFilePanel.add(loadBut);
        
        filePanel.add(new JLabel("Arquivo"));
        filePanel.add(inFilePanel);
        filePanel.add(fileName);
        
        //filePanel.setBorder(new  LineBorder(Color.BLACK));
        
        //////////////////
        //Playing Controllers
        JPanel playPanel = new JPanel(new GridLayout(4, 2, 4, 6));
        JButton reiniBut   = new JButton("Reiniciar");
        
        JPanel inPanel = new JPanel(new GridLayout(0, 2, 3, 3));
        BasicArrowButton playBut  = new BasicArrowButton(3, Color.LIGHT_GRAY, Color.black, Color.black, Color.black);
        JButton          pauseBut = new JButton("\u25A0");
        inPanel.add(pauseBut);
        inPanel.add(playBut);
        playPanel.add(new JLabel(""));
        playPanel.add(new JLabel("Execução"));
        playPanel.add(new JLabel(""));
        playPanel.add(reiniBut);
        playPanel.add(new JLabel(""));
        playPanel.add(inPanel);
        
        //////////////////
        //Simulation Speed
        
        JPanel speedPanel = new JPanel(new GridLayout(4, 1, 4, 6));
        
        JSlider speedSlide = new JSlider(JSlider.HORIZONTAL, 1, 20, inputs.getSpeed());
        speedSlide.setMinorTickSpacing(1);
        speedSlide.setMajorTickSpacing(5);
        speedSlide.setPaintTicks(true);
        
        speedPanel.add(new JLabel("Velocidade da Simulação"));
        speedPanel.add(speedSlide);
        speedPanel.add(new JLabel(""));
        speedPanel.add(new JLabel(""));
        
        //////////////////
        // Mutação
        JPanel mutPanel = new JPanel(new GridLayout(4, 2, 4, 6));
        JCheckBox mutColor = new JCheckBox("Cor");
        JCheckBox mutShape = new JCheckBox("Formato");
        JSlider colorSlide = new JSlider(JSlider.HORIZONTAL, 1, 10, inputs.getMut_color_press());
        JSlider shapeSlide = new JSlider(JSlider.HORIZONTAL, 1, 10, inputs.getMut_shape_press());
        colorSlide.setMinorTickSpacing(1);
        colorSlide.setMajorTickSpacing(5);
        colorSlide.setPaintTicks(true);
        shapeSlide.setMinorTickSpacing(1);
        shapeSlide.setMajorTickSpacing(5);
        shapeSlide.setPaintTicks(true);
        if(inputs.doMut_color())
            mutColor.doClick();
        else
            colorSlide.setEnabled(false);
        if(inputs.doMut_shape())
            mutShape.doClick();
        else
            shapeSlide.setEnabled(false);
        
        mutPanel.add(new JLabel("Mutação"));
        mutPanel.add(new JLabel(""));
        mutPanel.add(mutColor);
        mutPanel.add(mutShape);
        mutPanel.add(colorSlide);
        mutPanel.add(shapeSlide);
        
        
        //////////////////
        // Seleção
        JPanel selPanel = new JPanel(new GridLayout(4, 2, 4, 6));
        JCheckBox selResour = new JCheckBox("Recursos");
        JCheckBox selPred   = new JCheckBox("Predação");
        JSlider resourSlide = new JSlider(JSlider.HORIZONTAL, 1, 10, inputs.getSelec_food_press());
        JSlider predSlide   = new JSlider(JSlider.HORIZONTAL, 1, 10, inputs.getSelec_pred_press());
        resourSlide.setMinorTickSpacing(1);
        resourSlide.setMajorTickSpacing(5);
        resourSlide.setPaintTicks(true);
        predSlide.setMinorTickSpacing(1);
        predSlide.setMajorTickSpacing(5);
        predSlide.setPaintTicks(true);
        if(inputs.isSelec_food())
            selResour.doClick();
        else
            resourSlide.setEnabled(false);
        if(inputs.isSelec_pred())
            selPred.doClick();
        else
            predSlide.setEnabled(false);
        
        selPanel.add(new JLabel("Seleção"));
        selPanel.add(new JLabel(""));
        selPanel.add(selResour);
        selPanel.add(selPred);
        selPanel.add(resourSlide);
        selPanel.add(predSlide);
        
        //////////////////
        // Ambiente
        JPanel envPanel = new JPanel(new GridLayout(4, 3, 4, 6));
        JCheckBox envAntibio = new JCheckBox("Antibiótico");
        JCheckBox envTemp    = new JCheckBox("Temperatura");
        JCheckBox envPh      = new JCheckBox("PH");
        JSlider antibioSlide = new JSlider(JSlider.HORIZONTAL, 1, 10, inputs.getAntibio_press());
        JSlider tempSlide    = new JSlider(JSlider.HORIZONTAL, 1, 10, inputs.getTemper_press());
        JSlider phSlide      = new JSlider(JSlider.HORIZONTAL, 1, 10, inputs.getPh_press());
        antibioSlide.setMinorTickSpacing(1);
        antibioSlide.setMajorTickSpacing(5);
        antibioSlide.setPaintTicks(true);
        tempSlide.setMinorTickSpacing(1);
        tempSlide.setMajorTickSpacing(5);
        tempSlide.setPaintTicks(true);
        phSlide.setMinorTickSpacing(1);
        phSlide.setMajorTickSpacing(5);
        phSlide.setPaintTicks(true);
        if(inputs.isApply_antibio())
            envAntibio.doClick();
        else
            antibioSlide.setEnabled(false);
        if(inputs.isApply_temper())
            envTemp.doClick();
        else
            tempSlide.setEnabled(false);
        if(inputs.isApply_ph())
            envPh.doClick();
        else
            phSlide.setEnabled(false);
        
        envPanel.add(new JLabel("Ambiente"));
        envPanel.add(new JLabel(""));
        envPanel.add(new JLabel(""));
        envPanel.add(envAntibio);
        envPanel.add(envTemp);
        envPanel.add(envPh);
        envPanel.add(antibioSlide);
        envPanel.add(tempSlide);
        envPanel.add(phSlide);
        
        ///////////////////
        //Adds components
        JPanel fileAndPlayPanel = new JPanel(new GridLayout(1, 2, 8, 12));
        fileAndPlayPanel.add(filePanel);
        fileAndPlayPanel.add(playPanel);
        
        panel.add(fileAndPlayPanel);
        panel.add(speedPanel);
        panel.add(mutPanel);
        panel.add(selPanel);
        panel.add(envPanel);
        
        //////////////////////////////
        //Buttons actions performers
        
        saveBut.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == saveBut) {
                System.out.println("Salvar!");
            }
        }});
        
        loadBut.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == loadBut) {
                System.out.println("Carregar!");
            }
        }});
        
        reiniBut.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == reiniBut) {
                System.out.println("reiniciar!");
            }
        }});
        
        pauseBut.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == pauseBut) {
                System.out.println("pausar!");
            }
        }});
        
        playBut.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == playBut) {
                System.out.println("executar!");
            }
        }});
        
        mutColor.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == mutColor) {
                colorSlide.setEnabled(mutColor.isSelected());
            }
        }});
        
        mutShape.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == mutShape) {
                shapeSlide.setEnabled(mutShape.isSelected());
            }
        }});
        
        selResour.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == selResour) {
                resourSlide.setEnabled(selResour.isSelected());
            }
        }});
        
        selPred.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == selPred) {
                predSlide.setEnabled(selPred.isSelected());
            }
        }});
        
        envAntibio.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == envAntibio) {
                antibioSlide.setEnabled(envAntibio.isSelected());
            }
        }});
        
        envTemp.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == envTemp) {
                tempSlide.setEnabled(envTemp.isSelected());
            }
        }});
        
        envPh.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == envPh) {
                phSlide.setEnabled(envPh.isSelected());
            }
        }});
        
        
        ///////////////////////
        //Save or cancel
        int result = JOptionPane.showConfirmDialog(null, panel, "Configurar Cenário",
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        
        if (result == JOptionPane.OK_OPTION) {
            try{
                inputs.setSpeed(speedSlide.getValue());
                
                inputs.setMut_color(mutColor.isSelected());
                inputs.setMut_color_press(colorSlide.getValue());
                inputs.setMut_shape(mutShape.isSelected());
                inputs.setMut_shape_press(shapeSlide.getValue());
                
                inputs.setSelec_food(selResour.isSelected());
                inputs.setSelec_food_press(resourSlide.getValue());
                inputs.setSelec_pred(selPred.isSelected());
                inputs.setSelec_pred_press(predSlide.getValue());
                
                inputs.setApply_antibio(envAntibio.isSelected());
                inputs.setAntibio_press(antibioSlide.getValue());
                inputs.setApply_temper(envTemp.isSelected());
                inputs.setTemper_press(tempSlide.getValue());
                inputs.setApply_ph(envPh.isSelected());
                inputs.setPh_press(phSlide.getValue());
            }catch(NumberFormatException e){
                JOptionPane.showMessageDialog(null, "Verifique a entrada de pesos");
            }
        }
        
    }
    private void configurarAG(){
        JPanel panel = new JPanel(new GridLayout(0, 1, 10, 20));
        
        ///////////////////
        //File Management
        JPanel filePanel = new JPanel(new GridLayout(4, 1, 4, 6));
        JButton saveBut   = new JButton("Salvar");
        JButton loadBut = new JButton("Carregar");
        JTextField fileName = new JTextField("configuracao.agvl");
        
        JPanel inFilePanel = new JPanel(new GridLayout(1, 2, 3, 3));
        inFilePanel.add(saveBut);
        inFilePanel.add(loadBut);
        
        filePanel.add(new JLabel("Arquivo"));
        filePanel.add(inFilePanel);
        filePanel.add(fileName);
        
        
        /////////////////////////
        //Representação
        
        JPanel reprPanel = new JPanel(new GridLayout(4, 2, 4, 6));
        ButtonGroup reprGroup = new ButtonGroup();
        JRadioButton binaryRadio = new JRadioButton("Binário");
        binaryRadio.setActionCommand("Binário");
        JRadioButton realRadio   = new JRadioButton("Real");
        realRadio.setActionCommand("Real");
        
        switch(inputs.getRepresentation()){
            case "Binário":
                binaryRadio.setSelected(true);
                break;
            case "Real":
                realRadio.setSelected(true);
                break;
        }
        
        reprGroup.add(binaryRadio);
        reprGroup.add(realRadio);
        
        reprPanel.add(new JLabel(""));
        reprPanel.add(new JLabel("Representação"));
        reprPanel.add(new JLabel(""));
        reprPanel.add(binaryRadio);
        reprPanel.add(new JLabel(""));
        reprPanel.add(realRadio);
        
        //////////////////////////
        // Seleção Pais
        JPanel parSelPanel = new JPanel(new GridLayout(4, 1, 4, 6));
        ButtonGroup parSelGroup = new ButtonGroup();
        JRadioButton wheelRadio = new JRadioButton("Roleta");
        wheelRadio.setActionCommand("Roleta");
        JRadioButton tournRadio = new JRadioButton("Torneio");
        tournRadio.setActionCommand("Torneio");
        JRadioButton parUserRadio  = new JRadioButton("Usuário");
        parUserRadio.setActionCommand("Usuário");
        
        switch(inputs.getParentSelection()){
            case "Roleta":
                wheelRadio.setSelected(true);
                break;
            case "Torneio":
                tournRadio.setSelected(true);
                break;
            default:
                parUserRadio.setSelected(true);
        }
        
        parSelGroup.add(wheelRadio);
        parSelGroup.add(tournRadio);
        parSelGroup.add(parUserRadio);
        parSelPanel.add(new JLabel("Seleção de Pais"));
        parSelPanel.add(wheelRadio);
        parSelPanel.add(tournRadio);
        parSelPanel.add(parUserRadio);
        
        //////////////////////////
        // Seleção Sobreviventes
        JPanel surSelPanel = new JPanel(new GridLayout(4, 1, 4, 6));
        ButtonGroup surSelGroup = new ButtonGroup();
        JRadioButton geratRadio   = new JRadioButton("Geracional");
        geratRadio.setActionCommand("Geracional");
        JRadioButton mediumRadio  = new JRadioButton("Natural");
        mediumRadio.setActionCommand("Natural");
        JRadioButton surUserRadio = new JRadioButton("Usuário");
        surUserRadio.setActionCommand("Usuário");
        
        switch(inputs.getSurvivorSelection()){
            case "Geracional":
                geratRadio.setSelected(true);
                break;
            case "Natural":
                mediumRadio.setSelected(true);
                break;
            default:
                surUserRadio.setSelected(true);
        }
        
        surSelGroup.add(geratRadio);
        surSelGroup.add(mediumRadio);
        surSelGroup.add(surUserRadio);
        surSelPanel.add(new JLabel("Seleção de Sobreviventes"));
        surSelPanel.add(geratRadio);
        surSelPanel.add(mediumRadio);
        surSelPanel.add(surUserRadio);
        
        //////////////////////////////
        // Cruzamento
        JPanel crossPanel = new JPanel(new GridLayout(4, 1, 4, 6));
        
        final String[] crossOptionsBinary = {
            "Um Ponto",
            "Múltiplos Pontos",
            "Usuário"
        };
        
        final String[] crossOptionsReal = {
            "Um Ponto",
            "Múltiplos Pontos",
            "Aritmético",
            "Usuário"
        };
        
        JComboBox crossCombo;
        if(inputs.getRepresentation().equals("Binário"))
            crossCombo = new JComboBox(crossOptionsBinary);
        else
            crossCombo = new JComboBox(crossOptionsReal);
        
        crossCombo.setSelectedItem(inputs.getCrossover());
        
        int prob = (int) Math.round(10*inputs.getCrossProb());
        JSlider crossProbSlider = new JSlider(JSlider.HORIZONTAL, 1, 10, prob);
        
        crossPanel.add(new JLabel("Cruzamento"));
        crossPanel.add(crossCombo);
        crossPanel.add(crossProbSlider);
        
        //////////////////////////////
        // Mutação
        JPanel mutPanel = new JPanel(new GridLayout(4, 1, 4, 6));
        
        String[] mutOptionsBinary = {
            "Inversão",
            "Usuário"
        };
        
        String[] mutOptionsReal = {
            "Uniforme",
            "Gaussiana",
            "Usuário"
        };
        
        JComboBox mutCombo;
        
        if(inputs.getRepresentation().equals("Binário"))
            mutCombo = new JComboBox(mutOptionsBinary);
        else
            mutCombo = new JComboBox(mutOptionsReal);
            
        mutCombo.setSelectedItem(inputs.getMutation());
        
        prob = (int) Math.round(10*inputs.getMutProb());
        JSlider mutProbSlider = new JSlider(JSlider.HORIZONTAL, 1, 10, prob);
        
        mutPanel.add(new JLabel("Mutação"));
        mutPanel.add(mutCombo);
        mutPanel.add(mutProbSlider);
        
        //////////////////////////////
        // Fitness
        
        JPanel fitPanel = new JPanel(new GridLayout(4, 1, 4, 6));
        
        String[] fitOptions = {
            "Natural",
            "Usuário"
        };
        
        JComboBox fitCombo;
        fitCombo = new JComboBox(fitOptions);
            
        fitCombo.setSelectedItem(inputs.getFitness());
        
        fitPanel.add(new JLabel("Fitness"));
        fitPanel.add(fitCombo);
        fitPanel.add(new JLabel(""));
        fitPanel.add(new JLabel(""));
        
        ///////////////////////////////////////
        // Other
        
        JPanel otherPanel = new JPanel(new GridLayout(4, 2, 4, 6));
        
        JTextField maxGenField   = new JTextField(Integer.toString(inputs.getMaxGen()));
        JTextField popSizeField  = new JTextField(Integer.toString(inputs.getSizePop()));
        JTextField ringSizeField = new JTextField(Integer.toString(inputs.getRigSize()));
        
        maxGenField.setEnabled(false);
        popSizeField.setEnabled(false);
        ringSizeField.setEnabled(false);
        
        otherPanel.add(new JLabel("Outras Configurações"));
        otherPanel.add(new JLabel(""));
        
        otherPanel.add(new JLabel("Máx de Gerações"));
        otherPanel.add(maxGenField);
        otherPanel.add(new JLabel("Tamanho da População"));
        otherPanel.add(popSizeField);
        otherPanel.add(new JLabel("Tamanho do Ring"));
        otherPanel.add(ringSizeField);
        
        ////////////
        // Binaty to Real and vice-versa
        
        ActionListener reprOption = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(e.getSource() == binaryRadio) {
                    DefaultComboBoxModel model = new DefaultComboBoxModel(crossOptionsBinary);
                    crossCombo.setModel(model);
                    model = new DefaultComboBoxModel(mutOptionsBinary);
                    mutCombo.setModel(model);
                }else if(e.getSource() == realRadio){
                    DefaultComboBoxModel model = new DefaultComboBoxModel(crossOptionsReal);
                    crossCombo.setModel(model);
                    model = new DefaultComboBoxModel(mutOptionsReal);
                    mutCombo.setModel(model);
                }
            }
        };
        binaryRadio.addActionListener(reprOption);
        realRadio.addActionListener(reprOption);
        
        /////////////////////////
        // Main Panel
        JPanel fileAndReprPanel = new JPanel(new GridLayout(1, 2, 20, 12));
        fileAndReprPanel.add(filePanel);
        fileAndReprPanel.add(reprPanel);
        
        JPanel parAndSurPanel = new JPanel(new GridLayout(1, 2, 20, 12));
        parAndSurPanel.add(parSelPanel);
        parAndSurPanel.add(surSelPanel);
        
        JPanel crossAndMutPanel = new JPanel(new GridLayout(1, 2, 20, 12));
        crossAndMutPanel.add(crossPanel);
        crossAndMutPanel.add(mutPanel);
        
        JPanel fitAndOtherPanel = new JPanel(new GridLayout(1, 2, 20, 12));
        fitAndOtherPanel.add(fitPanel);
        fitAndOtherPanel.add(otherPanel);
        
        panel.add(fileAndReprPanel);
        panel.add(parAndSurPanel);
        panel.add(crossAndMutPanel);
        panel.add(fitAndOtherPanel);
        
        ///////////////////////
        //Save or cancel
        int result = JOptionPane.showConfirmDialog(null, panel, "Configurar Cenário",
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        
        if (result == JOptionPane.OK_OPTION) {
            try{
                inputs.setRepresentation(reprGroup.getSelection().getActionCommand());
                inputs.setCrossProb(((double)crossProbSlider.getValue())/10);
                inputs.setMutProb(((double)mutProbSlider.getValue())/10);
                inputs.setParentSelection(parSelGroup.getSelection().getActionCommand());
                inputs.setSurvivorSelection(surSelGroup.getSelection().getActionCommand());
                inputs.setCrossover(String.valueOf(crossCombo.getSelectedItem()));
                inputs.setMutation(String.valueOf(mutCombo.getSelectedItem()));
                inputs.setFitness(String.valueOf(fitCombo.getSelectedItem()));
                inputs.setSizePop(Integer.parseInt(popSizeField.getText()));
                inputs.setRigSize(Integer.parseInt(ringSizeField.getText()));
                inputs.setMaxGen(Integer.parseInt(maxGenField.getText()));
            }catch(NumberFormatException e){
                JOptionPane.showMessageDialog(null, "Verifique a entrada de pesos");
            }
        }
    }
    
    public void setInputs(Inputs inputs){
        this.inputs = inputs;
    }
    
    public Inputs getInputs(){
        return inputs;
    }
}

