
package bachome.GA.implementations;

import bachome.GA.Cromossome;
import bachome.GA.Gene;
import bachome.GA.Individual;
import bachome.GA.interfaces.Mutation;
import bachome.RandomPool;


public class UserMutation implements Mutation{
    
    private double probability;
    
    @Override
    public Individual doMutation(Individual ind, String shape){
        
        //You can implement this class
        
        return ind;
    }
    
    @Override
    public void setProbability(double probability){
        
        if(probability >= 0 && probability <= 1)
            this.probability = probability;
        else
            System.out.println("setProbability: invalid probability");
    }
    
    @Override
    public double getProbability(){
        
        return this.probability;
    }
}
