
package bachome.GA.implementations;

import bachome.GA.Cromossome;
import bachome.GA.Gene;
import bachome.GA.Individual;
import bachome.GA.GAUse;
import bachome.GA.interfaces.Crossover;
import bachome.RandomPool;

public class ArithmeticCrossover implements Crossover{
    
    private double probability;
    
    @Override
    public Individual doCrossover(Individual parent1, Individual parent2){
        
        Individual son = new Individual();
        son.copy(parent1);
        
        long IDs[] = new long[2];
        IDs[0] = parent1.getID();
        IDs[1] = parent2.getID();
        
        Cromossome crm1 = parent1.getCromossome();
        Cromossome crm2 = parent2.getCromossome();
        
        int nGenes         = crm1.getNGenes();
        double alpha       = RandomPool.r().nextDouble();
        Cromossome crmSon  = new Cromossome(nGenes);
        
        for(int i = 0; i < nGenes; i++){
            double val = alpha*crm1.getGene(i).getValue() + (1-alpha)*crm2.getGene(i).getValue();
            Gene g = new Gene(val);
            crmSon.setGene(g, i);
        }
        
        son.setCromossome(crmSon);
        son.setParentsID(IDs);
        son.setID(GAUse.assignID());
        son.setLifeTime(0);
        
        return son;
    }
    
    @Override
    public void setProbability(double probability){
        
        if(probability >= 0 && probability <= 1)
            this.probability = probability;
        else
            System.out.println("setProbability: invalid probability");
    }
    
    @Override
    public double getProbability(){
        
        return this.probability;
    }
}
